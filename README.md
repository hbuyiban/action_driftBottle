# 河北大学易班学生工作站TP5模板

#### 介绍
基于thinkphp5开发框架，用于河大易班内部php应用开发，可作为模板，引入了常用前端框架。

#### 前端资源列表
+ jquery
+ bootstrap
+ bootstrap-fileinput
+ qrcode
+ sb-admin框架
+ fontawesome字体

#### 使用教程

1. git clone https://gitee.com/hbuyiban/hbu-yiban-thinkphp5.git
2. 将内容复制到新建的项目下
3. 将新项目的git文件改为所在仓库的文件
4. public/index.php中配置APP_NAME，并根据实际情况选择本地开发还是上线
5. 配置config/app.index
6. 配置database.php(数据库配置)

#### 更新日志

+ 8.24
  + 项目初始化
  + 更改入口文件内容结构
  + 引入常用资源