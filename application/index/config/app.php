 <?php
    return [
        // 易班OpenApi
        // 上线的信息在187****2576的账号上
        'yiban_api'      =>  [
            'app_id'         => '',                          //此处填写你的appid
            'app_secret'     => '',          //此处填写你的AppSecret
            'call_back'      => '',              //此处填写你的授权回调地址
        ],
    ];
