<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

// 加载基础文件
require __DIR__ . '/../thinkphp/base.php';

// 支持事先使用静态方法设置Request对象和Config对象

//应用名称
define('APP_NAME', 'action_driftBottle');
/** 本地开发地址 */
define('WEB_DIR', 'http://localhost/' . APP_NAME . '/public/');
/** 服务器上线地址 */
//define('WEB_DIR', 'http://yiban.hbu.cn/' . APP_NAME . '/');
define('ROOT_DIR', str_replace('\\', '/', dirname(dirname(__FILE__))) . '/');
define('FILE_DIR', ROOT_DIR . 'public/static');
define('YIBAN_DEFAULT_RETURN_DIR', '');
// 执行应用并响应
Container::get('app')->run()->send();
