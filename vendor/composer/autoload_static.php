<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite17e69b85cdb7dc770d562493fa54269
{
    public static $prefixLengthsPsr4 = array (
        't' => 
        array (
            'think\\composer\\' => 15,
        ),
        'k' => 
        array (
            'kartik\\plugins\\fileinput\\' => 25,
        ),
        'a' => 
        array (
            'app\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'think\\composer\\' => 
        array (
            0 => __DIR__ . '/..' . '/topthink/think-installer/src',
        ),
        'kartik\\plugins\\fileinput\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/bootstrap-fileinput',
        ),
        'app\\' => 
        array (
            0 => __DIR__ . '/../..' . '/application',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite17e69b85cdb7dc770d562493fa54269::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite17e69b85cdb7dc770d562493fa54269::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite17e69b85cdb7dc770d562493fa54269::$classMap;

        }, null, ClassLoader::class);
    }
}
