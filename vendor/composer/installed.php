<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '93f6f624d4e8daf65f3c1eff93f0296a91793999',
    'name' => 'topthink/think',
  ),
  'versions' => 
  array (
    'kartik-v/bootstrap-fileinput' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.2.x-dev',
      ),
      'reference' => '9eebfd5c77629a0aa73a506dc22dde121c86c5fa',
    ),
    'topthink/framework' => 
    array (
      'pretty_version' => 'v5.1.41',
      'version' => '5.1.41.0',
      'aliases' => 
      array (
      ),
      'reference' => '7137741a323a4a60cfca334507cd1812fac91bb2',
    ),
    'topthink/think' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '93f6f624d4e8daf65f3c1eff93f0296a91793999',
    ),
    'topthink/think-installer' => 
    array (
      'pretty_version' => 'v2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '38ba647706e35d6704b5d370c06f8a160b635f88',
    ),
  ),
);
